$(function() {

	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});



});

var mySwiper = new Swiper ('.swiper-container', {
	// Optional parameters
	loop: true,
	slidesPerView: 2,
	spaceBetween: 500,
	// Navigation arrows
	navigation: {
		nextEl: '.next-1',
		prevEl: '.prev-1',
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
	},
	breakpoints: {
		768: {
			slidesPerView: 1,
			spaceBetween: 10,
			autoHeight: true
		}
	}
});

AOS.init({once: true});

// MENU 1
var menu = function(){
	$('.fa-bars').click(function(){
		$('.menu').animate({
			right: '0px'
		}, 200);

		$('.menu-icon').hide();

	});

	$('.menu-close').click(function(){
		$('.menu').animate({
			right: '-350px'
		}, 200);

		$('.menu-icon').show();

	});

};


$(document).ready(menu);

var btn = $('#button');

$(window).scroll(function() {
	if ($(window).scrollTop() > 300) {
		btn.addClass('show');
	} else {
		btn.removeClass('show');
	}
});

btn.on('click', function(e) {
	e.preventDefault();
	$('html, body').animate({scrollTop:0}, '300');
});


